public class Lion extends Animal {
    public Lion(String name, int length) {
        super(name, length, null);
        String type = "";
        if (length < 75){
            type = "A";
        }
        else if (length > 75 && length < 90){
            type = "B";
        }
        else{
            type = "C";
        }
        Cage cage = new Cage("indoor",type);
        super.setCage(cage);

    }

    public void hunt(){
        System.out.println("Lion is hunting..");
        System.out.println(this.getName() + " makes a voice: Err....");
    }

    public void goodMood(){
        System.out.println("Clean the lion's mane..");
        System.out.println(this.getName() + " makes a voice: Hauhhmm!");
    }

    public void disturbed(){
        System.out.println(this.getName() + " makes a voice: HAAHUM!!");
    }
}
