    import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class GamePanel {
    private ArrayList<GameCard> arrayGameCard = new ArrayList<>();
    private ArrayList<Integer> cardId = new ArrayList<>();
    private JPanel startPanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    private JFrame frame;
    private GameCard card1;
    private GameCard card2;
    private Timer timer;
    private Timer openTime;
    private Timer waitTime;
    private int attempts = 0;
    private JLabel attemptLabel;

    public GamePanel() {
        frame = new JFrame("Remember the flowers!");
        frame.setLayout(new BorderLayout());
        startPanel.setLayout(new GridLayout(6, 6));
        for (int i = 1; i < 19; i++) {
            cardId.add(i);
            cardId.add(i);
        }
        Collections.shuffle(cardId);
        for (int id : cardId) {
            //ImageIcon image1 = new ImageIcon("C:\\Users\\hp\\Documents\\UI\\STUDY\\DDP2\\ASSIGNMENT\\assignment\\assignment-4\\img\\wood.jpg");
             ImageIcon image1 = new ImageIcon("img/wood.jpg");
            //ImageIcon image2 = new ImageIcon("C:\\Users\\hp\\Documents\\UI\\STUDY\\DDP2\\ASSIGNMENT\\assignment\\assignment-4\\img" + id + ".jpg");
            ImageIcon image2 = new ImageIcon("img/" + id + ".jpg");
            image1 = resizeImage(image1);
            image2 = resizeImage(image2);
            GameCard newCard = new GameCard(id, image1, image2);
            newCard.getButton().addActionListener(actions -> turnCard(newCard));
            arrayGameCard.add(newCard);
        }
        createButton();
        createFooter();
        attemptLabel = new JLabel("Number of Attempts: " + attempts);
        attemptLabel.setFont(new Font(attemptLabel.getFont().getName(), Font.PLAIN, 16));
        frame.add(startPanel, BorderLayout.PAGE_START);
        frame.add(centerPanel, BorderLayout.CENTER);
        frame.add(attemptLabel, BorderLayout.PAGE_END);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setSize(600, 700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        waitTime = new Timer(2000, actions -> revealAll());
        waitTime.start();
        waitTime.setRepeats(false);
    }

    public void createButton() {
        for (GameCard i : arrayGameCard) {
            startPanel.add(i.getButton());
        }
    }

    public void createFooter() {
        JPanel footerPanel = new JPanel();
        footerPanel.setLayout(new GridLayout(1, 2));
        JButton play = new JButton("Play again");
        JButton exit = new JButton("Exit");
        footerPanel.add(play);
        footerPanel.add(exit);
        play.addActionListener(actions -> restartGame(arrayGameCard));
        exit.addActionListener(actions -> exitGame());
        centerPanel.add(footerPanel);
    }

    private void exitGame() {
        System.exit(0);
    }

    public void restartGame(ArrayList<GameCard> cards) {
        frame.remove(startPanel);
        Collections.shuffle(cards);
        startPanel = new JPanel(new GridLayout(6,6));
        for (GameCard i : cards) {
            i.setIsMatched(false);
            i.hide();
            i.getButton().setEnabled(true);
            startPanel.add(i.getButton());
        }
        frame.add(startPanel, BorderLayout.PAGE_START);
        attempts = 0;
        attemptLabel.setText("Number of Attempts: " + attempts);
        revealAll();
    }

    private void revealAll() {
        for (GameCard i: arrayGameCard) {
            i.reveal();
        }
        openTime = new Timer(2500, actions -> hideAll());
        openTime.setRepeats(false);
        openTime.start();
    }

    public void hideAll() {
        for (GameCard i : arrayGameCard) {
            i.hide();
        }
    }

    public void turnCard(GameCard current) {
        if (card1 == null && card2 == null) {
            card1 = current;
            card1.reveal();
        }

        if (card1 != null && card1 != current && card2 == null) {
            card2 = current;
            card2.reveal();
            timer = new Timer(700, actions -> checkGameCard());
            timer.setRepeats(false);
            timer.start();
        }
    }

    public void checkGameCard() {
        if (card1.getId() == card2.getId()) {
            matched();
            if (isFinished()) {
                Object[] choice = {" Quit Game", " Play Again, I love it!"};
                int option = JOptionPane.showOptionDialog(null, "Congratulations! You Win!", "Dialog Box",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, choice, choice[1]);
                if (option == 1) {
                    restartGame(arrayGameCard);
                } else {
                    exitGame();
                }
            }
        } else {
            card1.hide();
            card2.hide();
            attempts++;
            attemptLabel.setText("Number of Attempts: " + attempts);
        }
        card1 = null;
        card2 = null;
    }

    public void matched() {
        card1.getButton().setEnabled(false);
        card2.getButton().setEnabled(false);
        card1.setIsMatched(true);
        card2.setIsMatched(true);
    }

    public ImageIcon resizeImage(ImageIcon img) {
        Image i = img.getImage();
        Image resize = i.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        return new ImageIcon(resize);
    }

    public boolean isFinished() {
        for (GameCard i: arrayGameCard) {
            if (!i.getIsMatched()) {
                return false;
            }
        }
        return true;
    }

}