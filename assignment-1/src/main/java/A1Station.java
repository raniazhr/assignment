import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    public static TrainCar gerbong;
    static int kuciang;

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner kucing = new Scanner(System.in);
        kuciang = Integer.parseInt(kucing.nextLine());

        TrainCar hello = null;
        int counter = 0;

        for(int banyak = 0; banyak < kuciang; banyak++){
            String[] info = kucing.nextLine().split(",");
            String nama = (info[0]);
            int berat = Integer.parseInt(info[1]);
            int tinggi = Integer.parseInt(info[2]);
            counter++;

        WildCat cat = new WildCat(nama, berat, tinggi);
            if(hello==null){
                gerbong = new TrainCar(cat);
                hello = gerbong;
            }
            else{
                gerbong = new TrainCar(cat, gerbong);
                hello = gerbong;
            }

            if(gerbong.computeTotalWeight() > 250 || kuciang - 1 == banyak){
                prints(counter);
                hello = null;
                counter = 0;
            }
        }
    kucing.close();
    }
    public static void prints(int counter){
        double average = (gerbong.computeTotalMassIndex()/counter);

        System.out.println("The train departs to Javari Park"); 
        System.out.print("[LOCO]<--");
        gerbong.printCar();
        
        System.out.printf("Average mass index of all cats: %.2f\n", average);
        System.out.print("In average, the cats in the train are: ");

        if(average < 18.5){
            System.out.println("underweight");
        }
        else if(average >= 18.5 && average < 25){
            System.out.println("normal");
        }
        else if(average >= 25 && average < 30){
            System.out.println("overweight");
        }
        else if(average >= 30){
            System.out.println("obese");
        }

    }
}
