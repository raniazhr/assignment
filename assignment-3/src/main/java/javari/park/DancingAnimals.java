package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class DancingAnimals extends Attractions{
    
    private static String[] validAnimals = {"Parrot", "Snake", "Cat", "Hamster"};
    private ArrayList<String> listOfAnimals = new ArrayList<String>();

    public DancingAnimals(String name, String type){
        super(name, type);
    }

    public static boolean checkValidity(String type){
        for(int y = 0; y < validAnimals.length; y++){
            if(type.equals(validAnimals[y])){
                return true;
            }
        }

        return false;
    }

}