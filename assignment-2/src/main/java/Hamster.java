public class Hamster extends Animal{
    public Hamster(String name, int length) {
        super(name, length, null);
        String type = "";
        if (length < 45){
            type = "A";
        }
        else if (length > 45 && length < 60){
            type = "B";
        }
        else{
            type = "C";
        }
        Cage cage = new Cage("indoor",type);
        super.setCage(cage);
    }

    public void gnaw(){
        System.out.println(this.getName() + " makes a voice: Ngkkrit.. Ngkkrrriiit");
    }

    public void runInside(){
        System.out.println(this.getName() + " makes a voice: Trrr... Trrr...");
    }
}
