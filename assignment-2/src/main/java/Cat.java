
public class Cat extends Animal{
    private String[] voice = {"Miaaaw..","Purrr..","Mwaw!","Mraaawr!"};

    public Cat(String name, int length) {
        super(name, length, null);
        String type = "";
        if (length < 45){
            type = "A";
        }
        else if (length > 45 && length < 60){
            type = "B";
        }
        else{
            type = "C";
        }
        Cage cage = new Cage("indoor",type);
        super.setCage(cage);

    }

    public void brushFur(){
        System.out.println("Time to clean Katty's fur");
        System.out.println(this.getName() + " makes a voice: Nyaaan...");
    }

    public void cuddled(){
        String voice2 = voice[(int)(voice.length * Math.random())];
        System.out.println(this.getName() + " makes voice: " + voice2);
    }


}
