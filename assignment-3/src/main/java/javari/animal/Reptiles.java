package javari.animal;

import java.util.Scanner;
import javari.animal.*;
import java.util.Arrays;


public class Reptiles extends Animal{
    
    private boolean tame = false;
    private static String[] reptilia = {"Snake"};

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, String tamed, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(tamed.equals("tamed")){
            tame = true;
        }
    }

    public boolean specificCondition(){
        return !(tame);
    }

    public static String[] getReptilia(){
        return reptilia;
    }
}