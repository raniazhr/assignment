public class Animal {
    private String name;
    private int length;
    private Cage cage;

    public Animal(String name, int length, Cage cage) {
        super();
        this.name = name;
        this.length = length;
        this.cage = cage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Cage getCage() {
        return cage;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }

    //    public void inOutCage(String jenisHewan, int length){
//        if tipeHewan.equals("pet"){
//            if (length < 45){
//                this.setUkuranCage('A');
//            }
//            else if (length > 45 && length < 60){
//                this.setUkuranCage('B');
//            }
//            else{
//                this.setUkuranCage('C');
//            }
//        }
//        if tipeHewan.equals("wild"){
//            if (length < 75){
//                this.setUkuranCage('A');
//            }
//            else if (length > 75 && length < 90){
//                this.setUkuranCage('B');
//            }
//            else{
//                this.setUkuranCage('C');
//            }
//        }
//
//    }
}
