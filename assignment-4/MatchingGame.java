import javax.swing.*;

public class MatchingGame {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new GamePanel());
    }
}