import java.util.ArrayList;
import java.util.Scanner;

public class Test{
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        ArrayList<Cat> cats = new ArrayList<>();
        ArrayList<Hamster> hamsters = new ArrayList<>();
        ArrayList<Eagle> eagles = new ArrayList<>();
        ArrayList<Parrot> parrots = new ArrayList<>();
        ArrayList<Lion> lions = new ArrayList<>();
        ArrayList<Animal> animals = new ArrayList<>();


        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        System.out.print("Cat: ");
        int sumOfCats = Integer.parseInt(scan.nextLine());
        if (sumOfCats > 0){
            System.out.println("Provide the information of cat(s):");
            String catDatas[] = scan.nextLine().split(",");
            for (int i = 0; i < catDatas.length; i++) {
                String catData[] = catDatas[i].split("\\|");
                Cat cat = new Cat(catData[0], Integer.parseInt(catData[1]));
                cats.add(cat); //enter data to arraylist cats
                animals.add(cat); //enter data to arraylist animal
            }
        }
        System.out.print("Lion: ");
        int sumOfLions = Integer.parseInt(scan.nextLine());
        if (sumOfLions > 0){
            System.out.println("Provide the information of lion(s):");
            String lionDatas[] = scan.nextLine().split(",");
            for (int i = 0; i < lionDatas.length; i++) {
                String lionData[] = lionDatas[i].split("\\|");
                Lion lion = new Lion(lionData[0], Integer.parseInt(lionData[1]));
                lions.add(lion); //enter data ke arraylist lions
                animals.add(lion); //masukkin data ke arraylist animal
            }
        }
        System.out.print("Eagle: ");
        int sumOfEagles = Integer.parseInt(scan.nextLine());
        if (sumOfEagles > 0){
            System.out.println("Provide the information of eagle(s):");
            String eagleDatas[] = scan.nextLine().split(",");
            for (int i = 0; i < eagleDatas.length; i++) {
                String eagleData[] = eagleDatas[i].split("\\|");
                Eagle eagle = new Eagle(eagleData[0], Integer.parseInt(eagleData[1]));
                eagles.add(eagle); //masukkin data ke arraylist lions
                animals.add(eagle); //masukkin data ke arraylist animal
            }
        }
        System.out.print("Parrot: ");
        int sumOfParrots = Integer.parseInt(scan.nextLine());
        if (sumOfParrots > 0){
            System.out.println("Provide the information of parrot(s):");
            String parrotDatas[] = scan.nextLine().split(",");
            for (int i = 0; i < parrotDatas.length; i++) {
                String parrotData[] = parrotDatas[i].split("\\|");
                Parrot parrot = new Parrot(parrotData[0], Integer.parseInt(parrotData[1]));
                parrots.add(parrot); //masukkin data ke arraylist lions
                animals.add(parrot); //masukkin data ke arraylist animal
            }
        }
        System.out.print("Hamster: ");
        int sumOfhamsters = Integer.parseInt(scan.nextLine());
        if (sumOfhamsters > 0){
            System.out.println("Provide the information of hamster(s):");
            String hamsterDatas[] = scan.nextLine().split(",");
            for (int i = 0; i < hamsterDatas.length; i++) {
                String hamsterData[] = hamsterDatas[i].split("\\|");
                Hamster hamster = new Hamster(hamsterData[0], Integer.parseInt(hamsterData[1]));
                hamsters.add(hamster); //masukkin data ke arraylist lions
                animals.add(hamster); //masukkin data ke arraylist animal
            }
        }
        System.out.println("Animals have been successfully recorded!");
        System.out.println();
        System.out.println("=============================================");

        //arrangement

        System.out.println("Cage arrangement:");

        //arrangement untuk cats

        if (cats.size() > 0){
            int catCagesSize = cats.size() / 3;
            if (cats.size() < 3) catCagesSize = 1;
            Cat catCagesArrangement[][] = new Cat[3][catCagesSize + cats.size()];
            System.out.println("location: indoor");
            int counter = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < catCagesSize ; j++) {
                    if (counter < cats.size())
                    catCagesArrangement[i][j] = cats.get(counter);
                    counter++;
                }
            }
            int counter2 = catCagesSize;
            while(counter < cats.size()){
                catCagesArrangement[2][counter2] = cats.get(counter);
                counter++;
                counter2++;
            }
            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < catCagesArrangement[i].length; j++) {
                    if (catCagesArrangement[i][j] != null)
                        System.out.print(catCagesArrangement[i][j].getName() + " (" + catCagesArrangement[i][j].getLength() + " - " + catCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();

            //cat cages rearrangement

            System.out.println("After rearrangement...");
            Cat temp[] = catCagesArrangement[2];
            catCagesArrangement[2] = catCagesArrangement[1];
            catCagesArrangement[1] = catCagesArrangement[0];
            catCagesArrangement[0] = temp;

            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < catCagesSize / 2 ; j++) {
                    Cat tempCat = catCagesArrangement[i][j];
                    catCagesArrangement[i][j] = catCagesArrangement[i][catCagesSize - j - 1];
                    catCagesArrangement[i][catCagesSize - j - 1] = tempCat;
                }
            }

            int counterNotNull = 0;
            for (int i = 0; i < catCagesArrangement[0].length; i++) {
                if (catCagesArrangement[0][i] != null){
                    counterNotNull++;
                } else break;
            }
            for (int j = 0; j < counterNotNull / 2 ; j++) {
                Cat tempCat = catCagesArrangement[0][j];
                catCagesArrangement[0][j] = catCagesArrangement[0][counterNotNull - j - 1];
                catCagesArrangement[0][counterNotNull - j - 1] = tempCat;
            }

            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < catCagesArrangement[i].length; j++) {
                    if (catCagesArrangement[i][j] != null)
                        System.out.print(catCagesArrangement[i][j].getName() + " (" + catCagesArrangement[i][j].getLength() + " - " + catCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        }

        //====================================================================

        //arrangement untuk lions

        if (lions.size() > 0){
            int lionCagesSize = lions.size() / 3;
            if (lions.size() < 3) lionCagesSize = 1;
            Lion lionCagesArrangement[][] = new Lion[3][lionCagesSize + lions.size()];
            System.out.println("location: outdoor");
            int counter = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < lionCagesSize ; j++) {
                    if (counter < lions.size())
                    lionCagesArrangement[i][j] = lions.get(counter);
                    counter++;
                }
            }
            int counter2 = lionCagesSize;
            while(counter < lions.size()){
                lionCagesArrangement[2][counter2] = lions.get(counter);
                counter++;
                counter2++;
            }
            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < lionCagesArrangement[i].length; j++) {
                    if (lionCagesArrangement[i][j] != null)
                        System.out.print(lionCagesArrangement[i][j].getName() + " (" + lionCagesArrangement[i][j].getLength() + " - " + lionCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();

            //lion cages rearrangement

            System.out.println("After rearrangement...");
            Lion temp[] = lionCagesArrangement[2];
            lionCagesArrangement[2] = lionCagesArrangement[1];
            lionCagesArrangement[1] = lionCagesArrangement[0];
            lionCagesArrangement[0] = temp;

            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < lionCagesSize / 2 ; j++) {
                    Lion tempLion = lionCagesArrangement[i][j];
                    lionCagesArrangement[i][j] = lionCagesArrangement[i][lionCagesSize - j - 1];
                    lionCagesArrangement[i][lionCagesSize - j - 1] = tempLion;
                }
            }

            int counterNotNull = 0;
            for (int i = 0; i < lionCagesArrangement[0].length; i++) {
                if (lionCagesArrangement[0][i] != null){
                    counterNotNull++;
                } else break;
            }
            for (int j = 0; j < counterNotNull / 2 ; j++) {
                Lion tempLion = lionCagesArrangement[0][j];
                lionCagesArrangement[0][j] = lionCagesArrangement[0][counterNotNull - j - 1];
                lionCagesArrangement[0][counterNotNull - j - 1] = tempLion;
            }

            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < lionCagesArrangement[i].length; j++) {
                    if (lionCagesArrangement[i][j] != null)
                        System.out.print(lionCagesArrangement[i][j].getName() + " (" + lionCagesArrangement[i][j].getLength() + " - " + lionCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        }

        //====================================================================

        //arrangement untuk eagles

        if (eagles.size() > 0){
            int eagleCagesSize = eagles.size() / 3;
            if (eagles.size() < 3) eagleCagesSize = 1;
            Eagle eagleCagesArrangement[][] = new Eagle[3][eagleCagesSize + eagles.size()];
            System.out.println("location: outdoor");
            int counter = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < eagleCagesSize ; j++) {
                    if (counter < eagles.size())
                    eagleCagesArrangement[i][j] = eagles.get(counter);
                    counter++;
                }
            }
            int counter2 = eagleCagesSize;
            while(counter < eagles.size()){
                eagleCagesArrangement[2][counter2] = eagles.get(counter);
                counter++;
                counter2++;
            }
            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < eagleCagesArrangement[i].length; j++) {
                    if (eagleCagesArrangement[i][j] != null)
                        System.out.print(eagleCagesArrangement[i][j].getName() + " (" + eagleCagesArrangement[i][j].getLength() + " - " + eagleCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();

            //eagle cages rearrangement

            System.out.println("After rearrangement...");
            Eagle temp[] = eagleCagesArrangement[2];
            eagleCagesArrangement[2] = eagleCagesArrangement[1];
            eagleCagesArrangement[1] = eagleCagesArrangement[0];
            eagleCagesArrangement[0] = temp;

            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < eagleCagesSize / 2 ; j++) {
                    Eagle tempEagle = eagleCagesArrangement[i][j];
                    eagleCagesArrangement[i][j] = eagleCagesArrangement[i][eagleCagesSize - j - 1];
                    eagleCagesArrangement[i][eagleCagesSize - j - 1] = tempEagle;
                }
            }

            int counterNotNull = 0;
            for (int i = 0; i < eagleCagesArrangement[0].length; i++) {
                if (eagleCagesArrangement[0][i] != null){
                    counterNotNull++;
                } else break;
            }
            for (int j = 0; j < counterNotNull / 2 ; j++) {
                Eagle tempEagle = eagleCagesArrangement[0][j];
                eagleCagesArrangement[0][j] = eagleCagesArrangement[0][counterNotNull - j - 1];
                eagleCagesArrangement[0][counterNotNull - j - 1] = tempEagle;
            }

            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < eagleCagesArrangement[i].length; j++) {
                    if (eagleCagesArrangement[i][j] != null)
                        System.out.print(eagleCagesArrangement[i][j].getName() + " (" + eagleCagesArrangement[i][j].getLength() + " - " + eagleCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        }

        //====================================================================

        //arrangement untuk parrots

        if (parrots.size() > 0){
            int parrotCagesSize = parrots.size() / 3;
            if (parrots.size() < 3) parrotCagesSize = 1;
            Parrot parrotCagesArrangement[][] = new Parrot[3][parrotCagesSize + parrots.size()];
            System.out.println("location: indoor");
            int counter = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < parrotCagesSize ; j++) {
                    if (counter < parrots.size())
                    parrotCagesArrangement[i][j] = parrots.get(counter);
                    counter++;
                }
            }
            int counter2 = parrotCagesSize;
            while(counter < parrots.size()){
                parrotCagesArrangement[2][counter2] = parrots.get(counter);
                counter++;
                counter2++;
            }
            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < parrotCagesArrangement[i].length; j++) {
                    if (parrotCagesArrangement[i][j] != null)
                        System.out.print(parrotCagesArrangement[i][j].getName() + " (" + parrotCagesArrangement[i][j].getLength() + " - " + parrotCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();

            //parrot cages rearrangement

            System.out.println("After rearrangement...");
            Parrot temp[] = parrotCagesArrangement[2];
            parrotCagesArrangement[2] = parrotCagesArrangement[1];
            parrotCagesArrangement[1] = parrotCagesArrangement[0];
            parrotCagesArrangement[0] = temp;

            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < parrotCagesSize / 2 ; j++) {
                    Parrot tempParrot = parrotCagesArrangement[i][j];
                    parrotCagesArrangement[i][j] = parrotCagesArrangement[i][parrotCagesSize - j - 1];
                    parrotCagesArrangement[i][parrotCagesSize - j - 1] = tempParrot;
                }
            }

            int counterNotNull = 0;
            for (int i = 0; i < parrotCagesArrangement[0].length; i++) {
                if (parrotCagesArrangement[0][i] != null){
                    counterNotNull++;
                } else break;
            }
            for (int j = 0; j < counterNotNull / 2 ; j++) {
                Parrot tempParrot = parrotCagesArrangement[0][j];
                parrotCagesArrangement[0][j] = parrotCagesArrangement[0][counterNotNull - j - 1];
                parrotCagesArrangement[0][counterNotNull - j - 1] = tempParrot;
            }

            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < parrotCagesArrangement[i].length; j++) {
                    if (parrotCagesArrangement[i][j] != null)
                        System.out.print(parrotCagesArrangement[i][j].getName() + " (" + parrotCagesArrangement[i][j].getLength() + " - " + parrotCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        }

        //====================================================================

        //arrangement untuk hamsters

        if (hamsters.size() > 0){
            int hamsterCagesSize = hamsters.size() / 3;
            if (hamsters.size() < 3) hamsterCagesSize = 1;
            Hamster hamsterCagesArrangement[][] = new Hamster[3][hamsterCagesSize + hamsters.size()];
            System.out.println("location: indoor");
            int counter = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < hamsterCagesSize ; j++) {
                    if (counter < hamsters.size())
                    hamsterCagesArrangement[i][j] = hamsters.get(counter);
                    counter++;
                }
            }
            int counter2 = hamsterCagesSize;
            while(counter < hamsters.size()){
                hamsterCagesArrangement[2][counter2] = hamsters.get(counter);
                counter++;
                counter2++;
            }
            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < hamsterCagesArrangement[i].length; j++) {
                    if (hamsterCagesArrangement[i][j] != null)
                        System.out.print(hamsterCagesArrangement[i][j].getName() + " (" + hamsterCagesArrangement[i][j].getLength() + " - " + hamsterCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();

            //hamster cages rearrangement

            System.out.println("After rearrangement...");
            Hamster temp[] = hamsterCagesArrangement[2];
            hamsterCagesArrangement[2] = hamsterCagesArrangement[1];
            hamsterCagesArrangement[1] = hamsterCagesArrangement[0];
            hamsterCagesArrangement[0] = temp;

            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < hamsterCagesSize / 2 ; j++) {
                    Hamster tempHamster = hamsterCagesArrangement[i][j];
                    hamsterCagesArrangement[i][j] = hamsterCagesArrangement[i][hamsterCagesSize - j - 1];
                    hamsterCagesArrangement[i][hamsterCagesSize - j - 1] = tempHamster;
                }
            }

            int counterNotNull = 0;
            for (int i = 0; i < hamsterCagesArrangement[0].length; i++) {
                if (hamsterCagesArrangement[0][i] != null){
                    counterNotNull++;
                } else break;
            }
            for (int j = 0; j < counterNotNull / 2 ; j++) {
                Hamster tempHamster = hamsterCagesArrangement[0][j];
                hamsterCagesArrangement[0][j] = hamsterCagesArrangement[0][counterNotNull - j - 1];
                hamsterCagesArrangement[0][counterNotNull - j - 1] = tempHamster;
            }

            for (int i = 2; i >= 0 ; i--) {
                System.out.print("level " + (i + 1) + ": ");
                for (int j = 0; j < hamsterCagesArrangement[i].length; j++) {
                    if (hamsterCagesArrangement[i][j] != null)
                        System.out.print(hamsterCagesArrangement[i][j].getName() + " (" + hamsterCagesArrangement[i][j].getLength() + " - " + hamsterCagesArrangement[i][j].getCage().getType() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        }

        //====================================================================

        //number of each animal
        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:" + cats.size());
        System.out.println("lion:" + lions.size());
        System.out.println("parrot:" + parrots.size());
        System.out.println("eagle:" + eagles.size());
        System.out.println("hamster:" + hamsters.size());
        System.out.println();
        System.out.println("=============================================");


        //perlakuan to animal
        boolean finished = false;
        while (!finished){
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int animalChoose = Integer.parseInt(scan.nextLine());

            if (animalChoose == 1){
                System.out.print("Mention the name of cat you want to visit: ");
                String catName = scan.nextLine();
                boolean isExist = false;
                Cat selected = null;
                for (Cat cat: cats) {
                    if (cat.getName().equals(catName)){
                        isExist = true;
                        selected = cat;
                        break;
                    }
                }
                if (isExist){
                    System.out.println("You are visiting " + catName + " (cat) now, what would you like to do?");
                    System.out.println("1: Brush the fur 2: Cuddle");
                    int action = Integer.parseInt(scan.nextLine());
                    if (action == 1){
                        selected.brushFur();
                    } else if (action == 2){
                        selected.cuddled();
                    } else {
                        System.out.println("You do nothing...");
                    }
                } else{
                    System.out.print("There is no cat with that name! ");
                }
            } else if (animalChoose == 2){
                System.out.print("Mention the name of eagle you want to visit: ");
                String eagleName = scan.nextLine();
                boolean isExist = false;
                Eagle selected = null;
                for (Eagle eagle: eagles) {
                    if (eagle.getName().equals(eagleName)){
                        isExist = true;
                        selected = eagle;
                        break;
                    }
                }
                if (isExist){
                    System.out.println("You are visiting " + eagleName + " (eagle) now, what would you like to do?");
                    System.out.println("1: Order to fly");
                    int action = Integer.parseInt(scan.nextLine());
                    if (action == 1){
                        selected.flyHigh();
                    } else {
                        System.out.println("You do nothing...");
                    }
                } else{
                    System.out.print("There is no eagle with that name! ");
                }
            } else if (animalChoose == 3){
                System.out.print("Mention the name of hamster you want to visit: ");
                String hamsterName = scan.nextLine();
                boolean isExist = false;
                Hamster selected = null;
                for (Hamster hamster: hamsters) {
                    if (hamster.getName().equals(hamsterName)){
                        isExist = true;
                        selected = hamster;
                        break;
                    }
                }
                if (isExist){
                    System.out.println("You are visiting " + hamsterName + " (hamster) now, what would you like to do?");
                    System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                    int action = Integer.parseInt(scan.nextLine());
                    if (action == 1){
                        selected.gnaw();
                    } else if (action == 2){
                        selected.runInside();
                    } else {
                        System.out.println("You do nothing...");
                    }
                } else{
                    System.out.print("There is no hamster with that name! ");
                }
            } else if (animalChoose == 4){
                System.out.print("Mention the name of parrot you want to visit: ");
                String parrotName = scan.nextLine();
                boolean isExist = false;
                Parrot selected = null;
                for (Parrot parrot: parrots) {
                    if (parrot.getName().equals(parrotName)){
                        isExist = true;
                        selected = parrot;
                        break;
                    }
                }
                if (isExist){
                    System.out.println("You are visiting " + parrotName + " (parrot) now, what would you like to do?");
                    System.out.println("1: Order to fly 2: Do conversation");
                    int action = Integer.parseInt(scan.nextLine());
                    if (action == 1){
                        selected.fly();
                    } else if (action == 2){
                        System.out.print("You says: ");
                        String says = scan.nextLine();
                        selected.louder(says);
                    } else {
                        selected.doNotTalk();
                    }
                } else{
                    System.out.print("There is no parrot with that name! ");
                }
            } else if (animalChoose == 5){
                System.out.print("Mention the name of eagle you want to visit: ");
                String lionName = scan.nextLine();
                boolean isExist = false;
                Lion selected = null;
                for (Lion lion: lions) {
                    if (lion.getName().equals(lionName)){
                        isExist = true;
                        selected = lion;
                        break;
                    }
                }
                if (isExist){
                    System.out.println("You are visiting " + lionName + " (lion) now, what would you like to do?");
                    System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                    int action = Integer.parseInt(scan.nextLine());
                    if (action == 1){
                        selected.hunt();
                    } else if (action == 2){
                        selected.goodMood();
                    } else if (action == 3){
                        selected.disturbed();
                    } else {
                        System.out.println("You do nothing...");
                    }
                } else{
                    System.out.print("There is no lion with that name! ");
                }
            } else if (animalChoose == 99){
                finished = true;
            }

            if (animalChoose != 99){
                System.out.println("Back to the office!");
            }

            System.out.println();
        }
    scan.close();
    }
}
