public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    public WildCat cat;
    public TrainCar next;
    // TODO Complete me!

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if(next == null){
            return EMPTY_WEIGHT + cat.weight;
        }
        else{
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if(this.next == null){
            return cat.computeMassIndex();
        }
        else{
            return cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if(this.next == null){
            System.out.println("(" + this.cat.name + ")");
        }
        else{
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}
