package javari.animal;

import java.util.Scanner;
import javari.animal.*;
import java.util.Arrays;


public class Mammals extends Animal{
    
    private static String[] mammalia = {"Cat", "Whale", "Hamster", "Lion"};
    private boolean isPregnant = false;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, String preg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(preg.equalsIgnoreCase("pregnant")){
            this.isPregnant = true;
        }
    }

    public boolean specificCondition(){
        return !(isPregnant) && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
    }

    public static String[] getMamalia(){
        return mammalia;
    }

}