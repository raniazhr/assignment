public class Eagle extends Animal {
    public Eagle(String name, int length) {
        super(name, length, null);
        String type = "";
            if (length < 75){
                type = "A";
            }
            else if (length > 75 && length < 90){
                type = "B";
            }
            else{
                type = "C";
            }
        Cage cage = new Cage("indoor",type);
        super.setCage(cage);
    }


    public void flyHigh(){
        System.out.println(this.getName() + " makes a voice: Kwaakk....");
        System.out.println("You're hurt!");
    }
}
