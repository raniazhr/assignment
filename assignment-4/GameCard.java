import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GameCard {
    private int id;
    private ImageIcon closed;
    private ImageIcon revealed;
    private boolean isMatched = false;
    private JButton button = new JButton();

    public GameCard(int id, ImageIcon closed, ImageIcon revealed) {
        this.id = id;
        this.closed = closed;
        this.revealed = revealed;
        this.button.setIcon(closed);
        this.button.setPreferredSize(new Dimension(100, 100));
    }

    public void hide() {
        button.setIcon(closed);
    }

    public void reveal() {
        button.setIcon(revealed);
    }

//    public void isRevealed() {
//        button.setPreferredSize(new Dimension(100, 100));
//        if (isClicked) {
//            button.setIcon(revealed);
//        } else {
//            button.setIcon(closed);
//        }
//    }

    public void setIsMatched(boolean match) { this.isMatched = match; }

    public ImageIcon getClosed() { return closed; }

    public ImageIcon getRevealed() { return revealed; }

    public int getId() { return id; }

    public JButton getButton() { return button; }

    public boolean getIsMatched() { return isMatched; }
}