public class Parrot extends Animal {
    public Parrot(String name, int length) {
        super(name, length, null);
        String type = "";
        if (length < 45){
            type = "A";
        }
        else if (length > 45 && length < 60){
            type = "B";
        }
        else{
            type = "C";
        }
        Cage cage = new Cage("indoor",type);
        super.setCage(cage);
    }

    public void fly(){
        System.out.println("Parrot Greeny flies!");
        System.out.println(this.getName() + " make a voice: FLYYYY…..");
    }

    public void louder(String say){
        System.out.println("Grenny says: " + say.toUpperCase() );

    }
    public void doNotTalk(){
        System.out.println("Grenny says: HM?");

    }

}
